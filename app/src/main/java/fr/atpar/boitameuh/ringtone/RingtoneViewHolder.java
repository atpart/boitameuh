/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.net.Uri;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import fr.atpar.boitameuh.R;

import static android.view.View.GONE;
import static android.view.View.OnClickListener;
import static android.view.View.OnCreateContextMenuListener;
import static android.view.View.VISIBLE;

final class RingtoneViewHolder extends ItemViewHolder<RingtoneHolder>
        implements OnClickListener, OnCreateContextMenuListener {

    static final int VIEW_TYPE_SYSTEM_SOUND = R.layout.ringtone_item_sound;
    static final int VIEW_TYPE_CUSTOM_SOUND = -R.layout.ringtone_item_sound;
    static final int CLICK_NORMAL = 0;
    static final int CLICK_LONG_PRESS = -1;

    private final View mSelectedView;
    private final TextView mNameView;
    private final ImageView mImageView;

    RingtoneViewHolder(final View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        this.mSelectedView = itemView.findViewById(R.id.sound_image_selected);
        this.mNameView = itemView.findViewById(R.id.ringtone_name);
        this.mImageView = itemView.findViewById(R.id.ringtone_image);
    }

    @Override
    protected void onBindItemView(final RingtoneHolder itemHolder) {
        this.mNameView.setText(itemHolder.getName());
        final boolean opaque = itemHolder.isSelected();
        this.mNameView.setAlpha(opaque ? 1f : RingtoneConstants.ALPHA_VALUE_DISABLED_RINGTONE);
        this.mImageView.setAlpha(opaque ? 1f : RingtoneConstants.ALPHA_VALUE_DISABLED_RINGTONE);
        this.mImageView.clearColorFilter();

        final int itemViewType = this.getItemViewType();
        if (VIEW_TYPE_CUSTOM_SOUND == itemViewType) {
            this.mImageView.setImageResource(R.drawable.placeholder_album_artwork);
            // manage "remove" contextual menu
            this.itemView.setOnCreateContextMenuListener(this);
        } else if (Uri.EMPTY.equals(itemHolder.getItem())) {
            this.mImageView.setImageResource(R.drawable.ic_ringtone_silent);
        } else if (itemHolder.isPlaying()) {
            this.mImageView.setImageResource(R.drawable.ic_ringtone_active);
        } else {
            this.mImageView.setImageResource(R.drawable.ic_ringtone);
        }

        this.mSelectedView.setVisibility(itemHolder.isSelected() ? VISIBLE : GONE);

        final int bgColorId = itemHolder.isSelected() ? R.color.white_08p : R.color.transparent;
        this.itemView.setBackgroundColor(ContextCompat.getColor(this.itemView.getContext(), bgColorId));

    }

    @Override
    public void onClick(final View view) {
        this.notifyItemClicked(RingtoneViewHolder.CLICK_NORMAL);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu contextMenu, final View view,
                                    final ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.notifyItemClicked(RingtoneViewHolder.CLICK_LONG_PRESS);
        contextMenu.add(Menu.NONE, 0, Menu.NONE, R.string.remove_sound);
    }

}
