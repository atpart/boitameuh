package fr.atpar.boitameuh.ringtone;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.atpar.boitameuh.R;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;

/**
 * This activity presents a set of ringtones from which the user may select one. The set includes:
 * <ul>
 *     <li>system ringtones from the Android framework</li>
 *     <li>a ringtone representing pure silence</li>
 *     <li>a ringtone representing a default ringtone</li>
 *     <li>user-selected audio files available as ringtones</li>
 * </ul>
 */
@SuppressWarnings("ClassWithTooManyDependencies")
public class RingtonePickerActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<ItemHolder<Uri>>> {

    /**
     * To preview ringtone during selection.
     */
    private final RingtonePreviewKlaxon ringtonePreviewKlaxon = new RingtonePreviewKlaxon();
    /**
     * Stores the set of ItemHolders that wrap the selectable ringtones.
     */
    private ItemAdapter<ItemHolder<Uri>> mRingtoneAdapter;
    /**
     * The location of the custom ringtone to be removed.
     */
    private int mIndexOfRingtoneToRemove = RecyclerView.NO_POSITION;

    /**
     * The uri of the ringtone to select after data is loaded.
     */
    private Uri mSelectedRingtoneUri;

    /**
     * {@code true} indicates the {@link #mSelectedRingtoneUri} must be played after data load.
     */
    private boolean mIsPlaying;

    /**
     * To manage custom ringtones. Those provided by the application, but also user custom ringtones.
     */
    private RingtoneModel ringtoneModel;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_ringtone_picker);

        this.ringtoneModel = new RingtoneModel(this, PreferenceManager.getDefaultSharedPreferences(this));

        // retrieve selected element
        this.mSelectedRingtoneUri = this.ringtoneModel.getTimerRingtoneUri();

        final LayoutInflater inflater = this.getLayoutInflater();
        final OnItemClickedListener<ItemHolder<Uri>> listener = new ItemClickWatcher(this);
        final ItemViewHolderFactory ringtoneFactory = new RingtoneViewHolderFactory(inflater);
        final ItemViewHolderFactory headerFactory = new HeaderViewHolderFactory(inflater);
        final ItemViewHolderFactory addNewFactory = new AddCustomRingtoneViewHolderFactory(inflater);
        this.mRingtoneAdapter = new ItemAdapter<>();
        this.mRingtoneAdapter.withViewTypes(headerFactory, null, HeaderViewHolder.VIEW_TYPE_ITEM_HEADER)
                .withViewTypes(addNewFactory, listener, AddCustomRingtoneViewHolder.VIEW_TYPE_ADD_NEW)
                .withViewTypes(ringtoneFactory, listener, RingtoneViewHolder.VIEW_TYPE_SYSTEM_SOUND)
                .withViewTypes(ringtoneFactory, listener, RingtoneViewHolder.VIEW_TYPE_CUSTOM_SOUND);

        /*
         * Displays a set of selectable ringtones.
         */
        final RecyclerView mRecyclerView = this.findViewById(R.id.ringtone_content);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
        mRecyclerView.setAdapter(this.mRingtoneAdapter);
        mRecyclerView.setItemAnimator(null);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull final RecyclerView recyclerView, final int newState) {
                if (RecyclerView.NO_POSITION != RingtonePickerActivity.this.getIndexOfRingtoneToRemove()) {
                    RingtonePickerActivity.this.closeContextMenu();
                }
            }
        });

        LoaderManager.getInstance(this).initLoader(0 /* id */, null /* args */, this /* callback */);
        
        this.registerForContextMenu(mRecyclerView);
    }

    @Override
    protected void onPause() {
        if (null != this.mSelectedRingtoneUri) {
            // why do we update this only if not null ?
            this.ringtoneModel.setTimerRingtoneUri(this.mSelectedRingtoneUri);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (!this.isChangingConfigurations()) {
            this.stopPlayingRingtone(this.getSelectedRingtoneHolder(), false);
        }
        super.onStop();
    }

    @NonNull
    @Override
    public Loader<List<ItemHolder<Uri>>> onCreateLoader(final int id, @Nullable final Bundle args) {
        return new RingtoneLoader(this.getApplicationContext(), this.ringtoneModel);
    }

    @Override
    public void onLoadFinished(@NonNull final Loader<List<ItemHolder<Uri>>> loader, @Nullable final List<ItemHolder<Uri>> itemHolders) {
        // Update the adapter with fresh data.
        this.mRingtoneAdapter.setItems(itemHolders);

        // Attempt to select the requested ringtone.
        final RingtoneHolder toSelect = this.getRingtoneHolder(this.mSelectedRingtoneUri);
        if (null != toSelect) {
            toSelect.setSelected(true);
            this.mSelectedRingtoneUri = toSelect.getUri();
            toSelect.notifyItemChanged();

            // Start playing the ringtone if indicated.
            if (this.mIsPlaying) {
                this.startPlayingRingtone(toSelect);
            }
        } else {
            // Clear the selection since it does not exist in the data.
            this.ringtonePreviewKlaxon.stop(this);
            this.mSelectedRingtoneUri = null;
            this.mIsPlaying = false;
        }
    }

    @Override
    public void onLoaderReset(@NonNull final Loader<List<ItemHolder<Uri>>> loader) {
        // nothing to do.
    }

    RingtoneHolder getSelectedRingtoneHolder() {
        return this.getRingtoneHolder(this.mSelectedRingtoneUri);
    }

    @SuppressWarnings("WeakerAccess")
    int getIndexOfRingtoneToRemove() {
        return this.mIndexOfRingtoneToRemove;
    }

    void setIndexOfRingtoneToRemove(final int adapterPosition) {
        this.mIndexOfRingtoneToRemove = adapterPosition;
    }

    /**
     * The given {@code ringtone} will be selected as a side-effect of playing the ringtone.
     *
     * @param ringtone the ringtone to be played
     */
    void startPlayingRingtone(final RingtoneHolder ringtone) {
        if (!ringtone.isPlaying() && !ringtone.isSilent()) {
            this.ringtonePreviewKlaxon.start(this.getApplicationContext(), ringtone.getUri());
            ringtone.setPlaying(true);
            this.mIsPlaying = true;
        }
        if (!ringtone.isSelected()) {
            ringtone.setSelected(true);
            this.mSelectedRingtoneUri = ringtone.getUri();
        }
        ringtone.notifyItemChanged();
    }

    /**
     * @param ringtone the ringtone to stop playing
     * @param deselect {@code true} indicates the ringtone should also be deselected; {@code false} indicates its selection state should remain unchanged
     */
    void stopPlayingRingtone(@NonNull final RingtoneHolder ringtone, final boolean deselect) {
        if (null != ringtone) {
            if (ringtone.isPlaying()) {
                this.ringtonePreviewKlaxon.stop(this);
                ringtone.setPlaying(false);
                this.mIsPlaying = false;
            }
            if (deselect && ringtone.isSelected()) {
                ringtone.setSelected(false);
                this.mSelectedRingtoneUri = null;
            }
            ringtone.notifyItemChanged();
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull final MenuItem item) {
        // Find the ringtone to be removed.
        final List<ItemHolder<Uri>> items = this.mRingtoneAdapter.getItems();
        final RingtoneHolder toRemove = (RingtoneHolder) items.get(this.mIndexOfRingtoneToRemove);
        this.mIndexOfRingtoneToRemove = RecyclerView.NO_POSITION;

        // Launch the confirmation dialog.
//        final boolean hasPermissions = toRemove.hasPermissions();
        // start asynchronous remove of selected ringtone.
        new RemoveCustomRingtoneTask(this, toRemove.getUri()).execute();
        //ConfirmRemoveCustomRingtoneDialogFragment.show(this, toRemove.getUri(), hasPermissions);
        return true;
    }

    /**
     * Proceeds with removing the custom ringtone with the given uri. Called from asynchronous task {@link RemoveCustomRingtoneTask}.
     *
     * @param uriToRemove identifies the custom ringtone to be removed
     */
    void removeCustomRingtone(final Uri uriToRemove) {
        // Reset the timer ringtone if it was just removed.
        if (uriToRemove.equals(this.ringtoneModel.getTimerRingtoneUri())) {
            final Uri timerRingtoneUri = this.ringtoneModel.getDefaultTimerRingtoneUri();
            this.ringtoneModel.setTimerRingtoneUri(timerRingtoneUri);
        }

        // Remove the corresponding custom ringtone.
        this.ringtoneModel.removeCustomRingtone(uriToRemove);

        // update now the user interface ...
        // Find the ringtone to be removed from the adapter.
        final RingtoneHolder toRemove = this.getRingtoneHolder(uriToRemove);
        if (null != toRemove) {
            // If the ringtone to remove is also the selected ringtone, adjust the selection.
            if (toRemove.isSelected()) {
                this.stopPlayingRingtone(toRemove, false);
                final RingtoneHolder defaultRingtone = this.getRingtoneHolder(this.ringtoneModel.getDefaultTimerRingtoneUri());
                if (null != defaultRingtone) {
                    defaultRingtone.setSelected(true);
                    this.mSelectedRingtoneUri = defaultRingtone.getUri();
                    defaultRingtone.notifyItemChanged();
                }
            }

            // Remove the ringtone from the adapter.
            this.mRingtoneAdapter.removeItem(toRemove);
        }
    }

    private RingtoneHolder getRingtoneHolder(final Uri uri) {
        RingtoneHolder result = null;
        for (final ItemHolder<Uri> itemHolder : this.mRingtoneAdapter.getItems()) {
            if (itemHolder instanceof RingtoneHolder) {
                final RingtoneHolder ringtoneHolder = (RingtoneHolder) itemHolder;
                if (ringtoneHolder.getUri().equals(uri)) {
                    result = ringtoneHolder;
                    // stop loop
                    break;
                }
            }
        }

        return result;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RESULT_OK == resultCode && null != data) {
            final Uri uri = data.getData();
            // Bail if the permission to read (playback) the audio at the uri was not granted.
            final int flags = data.getFlags() & FLAG_GRANT_READ_URI_PERMISSION;
            if (null != uri && FLAG_GRANT_READ_URI_PERMISSION == flags) {
                // Start a task to fetch the display name of the audio content and add the custom ringtone.
                new AddCustomRingtoneTask(this, uri).execute();
            }
        }
    }

    void addCustomRingtone(final Uri mUri, final String title) {
        // Add the new custom ringtone to the data model.
        this.ringtoneModel.addCustomRingtone(mUri, title);

        // When the loader completes, it must play the new ringtone.
        this.mSelectedRingtoneUri = mUri;
        this.mIsPlaying = true;

        // Reload the data to reflect the change in the UI.
        LoaderManager.getInstance(this).restartLoader(0 /* id */, null /* args */,
                this /* callback */);
    }
}
