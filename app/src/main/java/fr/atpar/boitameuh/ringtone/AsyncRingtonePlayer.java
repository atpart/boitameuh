package fr.atpar.boitameuh.ringtone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

/**
 * <p>This class controls playback of ringtones. Uses {@link Ringtone} or {@link MediaPlayer} in a
 * dedicated thread so that this class can be called from the main thread. Consequently, problems
 * controlling the ringtone do not cause ANRs in the main thread of the application.</p>
 *
 * <p>This class also serves a second purpose. It accomplishes alarm ringtone playback using two
 * different mechanisms depending on the underlying platform.</p>
 *
 * <ul>
 *     <li>Prior to the M platform release, ringtone playback is accomplished using
 *     {@link MediaPlayer}. android.permission.READ_EXTERNAL_STORAGE is required to play custom
 *     ringtones located on the SD card using this mechanism. {@link MediaPlayer} allows clients to
 *     adjust the volume of the stream and specify that the stream should be looped.</li>
 *
 *     <li>Starting with the M platform release, ringtone playback is accomplished using
 *     {@link Ringtone}. android.permission.READ_EXTERNAL_STORAGE is <strong>NOT</strong> required
 *     to play custom ringtones located on the SD card using this mechanism. {@link Ringtone} allows
 *     clients to adjust the volume of the stream and specify that the stream should be looped but
 *     those methods are marked @hide in M and thus invoked using reflection. Consequently, revoking
 *     the android.permission.READ_EXTERNAL_STORAGE permission has no effect on playback in M+.</li>
 * </ul>
 *
 * <p>If either the {@link Ringtone} or {@link MediaPlayer} fails to play the requested audio, an
 * {@link Utils#getFallbackRingtoneUri in-app fallback} is used because playing <strong>some</strong>
 * sort of noise is always preferable to remaining silent.</p>
 */
@SuppressWarnings("OverlyComplexAnonymousInnerClass")
public class AsyncRingtonePlayer {

    // Message codes used with the ringtone thread.
    private static final int EVENT_PLAY = 1;
    private static final int EVENT_STOP = 2;
    private static final String RINGTONE_URI_KEY = "RINGTONE_URI_KEY";

    /**
     * The context.
     */
    private final Context mContext;

    /**
     * Handler running on the ringtone thread.
     */
    private Handler mHandler;
    /**
     * {@link RingtonePlaybackDelegate} for every version of android.
     */
    private PlaybackDelegate mPlaybackDelegate;

    public AsyncRingtonePlayer(@NonNull final Context context) {
        this.mContext = context;
    }


    @SuppressWarnings("WeakerAccess")
    Context getContext() {
        return this.mContext;
    }

    /**
     * Plays the ringtone.
     */
    public void play(@NonNull final Uri ringtoneUri) {
        this.postMessage(EVENT_PLAY, ringtoneUri);
    }

    /**
     * Stops playing the ringtone.
     */
    public void stop() {
        this.postMessage(EVENT_STOP, null);
    }

    /**
     * Posts a message to the ringtone-thread handler.
     *
     * @param messageCode the message to post
     * @param ringtoneUri the ringtone in question, if any
     */
    private void postMessage(final int messageCode, final Uri ringtoneUri) {
        synchronized (this) {
            if (null == this.mHandler) {
                this.mHandler = this.getNewHandler();
            }

            final Message message = this.mHandler.obtainMessage(messageCode);
            if (null != ringtoneUri) {
                final Bundle bundle = new Bundle();
                bundle.putParcelable(RINGTONE_URI_KEY, ringtoneUri);
                message.setData(bundle);
            }

            this.mHandler.sendMessageAtFrontOfQueue(message);
        }
    }

    /**
     * Creates a new ringtone Handler running in its own thread.
     */
    @SuppressLint("HandlerLeak")
    private Handler getNewHandler() {
        final HandlerThread thread = new HandlerThread("ringtone-player");
        thread.start();

        return new Handler(thread.getLooper()) {
            @Override
            public void handleMessage(@SuppressWarnings("NullableProblems") final Message msg) {
                if (null != msg) {
                    switch (msg.what) {
                        case EVENT_PLAY:
                            final Bundle data = msg.getData();
                            final Uri ringtoneUri = data.getParcelable(RINGTONE_URI_KEY);
                            AsyncRingtonePlayer.this.getPlaybackDelegate().play(AsyncRingtonePlayer.this.getContext(), ringtoneUri);
                            break;
                        case EVENT_STOP:
                            AsyncRingtonePlayer.this.getPlaybackDelegate().stop();
                            break;
                    }
                }
            }
        };
    }

    /**
     * Check if the executing thread is the one dedicated to controlling the ringtone playback.
     */
    private void checkAsyncRingtonePlayerThread() {
        if (Looper.myLooper() != this.mHandler.getLooper()) {
            Log.e("AsyncRingtonePlayer", "Must be on the AsyncRingtonePlayer thread!",
                    new IllegalStateException());
        }
    }

    /**
     * @return the platform-specific playback delegate to use to play the ringtone
     */
    @SuppressWarnings("WeakerAccess")
    PlaybackDelegate getPlaybackDelegate() {
        this.checkAsyncRingtonePlayerThread();

        if (null == this.mPlaybackDelegate) {
            // Use the newer Ringtone-based playback delegate because it does not require
            // any permissions to read from the SD card. (M+)
            this.mPlaybackDelegate = new RingtonePlaybackDelegate();
        }

        return this.mPlaybackDelegate;
    }


}