package fr.atpar.boitameuh.ringtone;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

/**
 * Loops playback of a ringtone using {@link Ringtone}.
 */
class RingtonePlaybackDelegate implements PlaybackDelegate {

    /**
     * The audio focus manager. Only used by the ringtone thread.
     */
    private AudioManager mAudioManager;

    /**
     * The current ringtone. Only used by the ringtone thread.
     */
    private Ringtone mRingtone;
    private AudioFocusRequest audioFocusRequest;

    /**
     * Starts the actual playback of the ringtone. Executes on ringtone-thread.
     */
    @Override
    public void play(@NonNull final Context context, @NonNull Uri ringtoneUri) {
        if (null == this.mAudioManager) {
            this.mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        }

        // Attempt to fetch the specified ringtone.
        this.mRingtone = RingtoneManager.getRingtone(context, ringtoneUri);

        // If no ringtone exists at this point there isn't much recourse.
        if (null == this.mRingtone) {
            ringtoneUri = Utils.getFallbackRingtoneUri(context);
            this.mRingtone = RingtoneManager.getRingtone(context, ringtoneUri);
        }

        try {
            this.startPlayback();
        } catch (final Throwable t) {
            Log.e("RingtonePlayback", "Using the fallback ringtone, could not play " + ringtoneUri, t);
            // At this point we just don't play anything.
        }
    }

    /**
     * Prepare the Ringtone for playback, then start the playback.
     */
    private void startPlayback() {
        // Indicate the ringtone should be played via the alarm stream.
        if (Utils.isLOrLater()) {
            this.mRingtone.setAudioAttributes(new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build());
        }

        if (Utils.isOOrLater()) {
            final AudioFocusRequest.Builder audioFocusRequestBuilder = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN);
            this.audioFocusRequest = audioFocusRequestBuilder.build();
            this.mAudioManager.requestAudioFocus(this.audioFocusRequest);
        }
        this.mRingtone.play();
    }

    /**
     * Stops the playback of the ringtone. Executes on the ringtone-thread.
     */
    @Override
    public void stop() {
        if (null != this.mRingtone && this.mRingtone.isPlaying()) {
            this.mRingtone.stop();
        }

        this.mRingtone = null;

        if (Utils.isOOrLater() && null != this.mAudioManager && null != this.audioFocusRequest) {
            this.mAudioManager.abandonAudioFocusRequest(this.audioFocusRequest);
        }
    }
}
