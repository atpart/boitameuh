/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.AnyRes;
import androidx.annotation.NonNull;

import fr.atpar.boitameuh.R;

public class Utils {

    /**
     * @return {@code true} if the device is {@link Build.VERSION_CODES#LOLLIPOP} or later
     */
    public static boolean isLOrLater() {
        return Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT;
    }

    /**
     * @return {@code true} if the device is {@link Build.VERSION_CODES#O} or later
     */
    public static boolean isOOrLater() {
        return Build.VERSION_CODES.O <= Build.VERSION.SDK_INT;
    }

    /**
     * @param resourceId identifies an application resource
     * @return the Uri by which the application resource is accessed
     */
    @NonNull
    public static
    Uri getResourceUri(@NonNull final Context context, @AnyRes final int resourceId) {
        return new Uri.Builder()
                .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                .authority(context.getPackageName())
                .path(String.valueOf(resourceId))
                .build();
    }

    /**
     * @return Uri of the ringtone to play when the chosen ringtone fails to play
     */
    static Uri getFallbackRingtoneUri(final Context context) {
        return Utils.getResourceUri(context, R.raw.hero_decorative_celebration_03);
    }
}