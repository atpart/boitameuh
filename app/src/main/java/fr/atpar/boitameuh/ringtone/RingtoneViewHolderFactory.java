package fr.atpar.boitameuh.ringtone;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import fr.atpar.boitameuh.R;

public class RingtoneViewHolderFactory implements ItemViewHolderFactory<RingtoneHolder> {

    private final LayoutInflater mInflater;

    RingtoneViewHolderFactory(final LayoutInflater inflater) {
        this.mInflater = inflater;
    }

    @NonNull
    @Override
    public ItemViewHolder<RingtoneHolder> createViewHolder(@Nullable final ViewGroup parent, final int viewType) {
        final View itemView = this.mInflater.inflate(R.layout.ringtone_item_sound, parent, false);
        return new RingtoneViewHolder(itemView);
    }
}
