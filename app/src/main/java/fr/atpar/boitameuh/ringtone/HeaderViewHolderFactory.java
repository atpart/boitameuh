package fr.atpar.boitameuh.ringtone;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class HeaderViewHolderFactory implements ItemViewHolderFactory<HeaderHolder> {

    private final LayoutInflater mInflater;

    HeaderViewHolderFactory(final LayoutInflater inflater) {
        this.mInflater = inflater;
    }

    @NonNull
    @Override
    public ItemViewHolder<HeaderHolder> createViewHolder(@Nullable final ViewGroup parent, final int viewType) {
        return new HeaderViewHolder(this.mInflater.inflate(viewType, parent, false));
    }
}
