/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.ArrayMap;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import fr.atpar.boitameuh.R;

import static android.media.AudioManager.STREAM_ALARM;
import static android.media.RingtoneManager.TITLE_COLUMN_INDEX;

/**
 * All ringtone data is accessed via this model.
 */
public class RingtoneModel {

    private final Context mContext;

    private final SharedPreferences mPrefs;

    /**
     * Maps ringtone uri to ringtone title; looking up a title from scratch is expensive.
     */
    private final Map<Uri, String> mRingtoneTitles = new ArrayMap<>(16);

    /**
     * A mutable copy of the custom ringtones.
     */
    private List<CustomRingtone> mCustomRingtones;

    /**
     * The uri of the default ringtone to use for timers until the user explicitly chooses one.
     */
    private Uri mDefaultTimerRingtoneUri;

    public RingtoneModel(@NonNull final Context context, @NonNull final SharedPreferences prefs) {
        this.mContext = context;
        this.mPrefs = prefs;
    }

    void addCustomRingtone(final Uri uri, final String title) {
        // If the uri is already present in an existing ringtone, do nothing.
        final CustomRingtone existing = this.getCustomRingtone(uri);
        if (null == existing) {
            final CustomRingtone ringtone = CustomRingtoneDAO.addCustomRingtone(this.mPrefs, uri, title);
            this.getMutableCustomRingtones().add(ringtone);
            Collections.sort(this.getMutableCustomRingtones());
        }
    }

    void removeCustomRingtone(final Uri uri) {
        final List<CustomRingtone> ringtones = this.getMutableCustomRingtones();
        for (final CustomRingtone ringtone : ringtones) {
            if (ringtone.getUri().equals(uri)) {
                CustomRingtoneDAO.removeCustomRingtone(this.mPrefs, ringtone.getId());
                ringtones.remove(ringtone);
                break;
            }
        }
    }

    private CustomRingtone getCustomRingtone(final Uri uri) {
        CustomRingtone customRingtone = null;
        for (final CustomRingtone ringtone : this.getMutableCustomRingtones()) {
            if (ringtone.getUri().equals(uri)) {
                customRingtone = ringtone;
                // quit the loop
                break;
            }
        }
        return customRingtone;
    }

    List<CustomRingtone> getCustomRingtones() {
        return Collections.unmodifiableList(this.getMutableCustomRingtones());
    }

    void loadRingtoneTitles() {
        // Early return if the cache is already primed.
        if (this.mRingtoneTitles.isEmpty()) {
            final RingtoneManager ringtoneManager = new RingtoneManager(this.mContext);
            ringtoneManager.setType(STREAM_ALARM);

            // Cache a title for each system ringtone.
            try (final Cursor cursor = ringtoneManager.getCursor()) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    final String ringtoneTitle = cursor.getString(TITLE_COLUMN_INDEX);
                    final Uri ringtoneUri = ringtoneManager.getRingtoneUri(cursor.getPosition());
                    this.mRingtoneTitles.put(ringtoneUri, ringtoneTitle);
                }
            } catch (final Throwable throwable) {
                // best attempt only
                Log.e("RingtoneModel", "Error loading ringtone title cache", throwable);
            }
        }
    }

    String getRingtoneTitle(final Uri uri) {
        String title;
        // Special case: no ringtone has a title of "Silent".
        if (Uri.EMPTY.equals(uri)) {
            title = this.mContext.getString(R.string.silent_ringtone_title);
        } else {

            // If the ringtone is custom, it has its own title.
            final CustomRingtone customRingtone = this.getCustomRingtone(uri);
            if (null != customRingtone) {
                title = customRingtone.getTitle();
            } else {
                // Check the cache.
                title = this.mRingtoneTitles.get(uri);

                if (null == title) {
                    // This is slow because a media player is created during Ringtone object creation.
                    final Ringtone ringtone = RingtoneManager.getRingtone(this.mContext, uri);
                    if (null == ringtone) {
                        Log.e("RingtoneModel", "No ringtone for uri: " + uri);
                        title = this.mContext.getString(R.string.unknown_ringtone_title);
                    } else {
                        // Cache the title for later use.
                        title = ringtone.getTitle(this.mContext);
                        this.mRingtoneTitles.put(uri, title);
                    }
                }
            }
        }
        return title;
    }

    private List<CustomRingtone> getMutableCustomRingtones() {
        if (null == this.mCustomRingtones) {
            this.mCustomRingtones = CustomRingtoneDAO.getCustomRingtones(this.mPrefs);
            Collections.sort(this.mCustomRingtones);
        }

        return this.mCustomRingtones;
    }

    Uri getDefaultTimerRingtoneUri() {
        if (null == this.mDefaultTimerRingtoneUri) {
            this.mDefaultTimerRingtoneUri = Utils.getResourceUri(this.mContext, R.raw.meuglement_0546_cut2);
        }

        return this.mDefaultTimerRingtoneUri;
    }

    @NonNull
    public Uri getTimerRingtoneUri() {
        return CustomRingtoneDAO.getTimerRingtoneUri(this.mPrefs, this.getDefaultTimerRingtoneUri());
    }

    void setTimerRingtoneUri(final Uri uri) {
        CustomRingtoneDAO.setTimerRingtoneUri(this.mPrefs, uri);
    }

}