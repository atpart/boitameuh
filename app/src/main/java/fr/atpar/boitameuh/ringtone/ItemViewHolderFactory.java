package fr.atpar.boitameuh.ringtone;

import android.view.ViewGroup;

/**
 * Factory interface used by {@link ItemAdapter} for creating new {@link ItemViewHolder}.
 */
interface ItemViewHolderFactory<T extends ItemHolder> {
    /**
     * Used by {@link ItemAdapter#createViewHolder(ViewGroup, int)} to make new
     * {@link ItemViewHolder} for a given view type.
     *
     * @param parent   the {@code ViewGroup} that the {@link ItemViewHolder#itemView} will
     *                 be attached
     * @param viewType the unique id of the item view to create
     * @return a new initialized {@link ItemViewHolder}
     */
    ItemViewHolder<T> createViewHolder(ViewGroup parent, int viewType);
}
