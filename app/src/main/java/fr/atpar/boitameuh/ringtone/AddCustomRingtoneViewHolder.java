/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.atpar.boitameuh.R;

import static android.view.View.GONE;

final class AddCustomRingtoneViewHolder extends ItemViewHolder<AddCustomRingtoneHolder>
        implements View.OnClickListener {

    static final int VIEW_TYPE_ADD_NEW = Integer.MIN_VALUE;
    static final int CLICK_ADD_NEW = VIEW_TYPE_ADD_NEW;

    AddCustomRingtoneViewHolder(final View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        final View selectedView = itemView.findViewById(R.id.sound_image_selected);
        selectedView.setVisibility(GONE);

        final TextView nameView = itemView.findViewById(R.id.ringtone_name);
        nameView.setText(itemView.getContext().getString(R.string.add_new_sound));
        nameView.setAlpha(RingtoneConstants.ALPHA_VALUE_DISABLED_RINGTONE);

        final ImageView imageView = itemView.findViewById(R.id.ringtone_image);
        imageView.setImageResource(R.drawable.ic_add_black_24dp);
        imageView.setAlpha(RingtoneConstants.ALPHA_VALUE_DISABLED_RINGTONE);
    }

    @Override
    public void onClick(final View view) {
        this.notifyItemClicked(AddCustomRingtoneViewHolder.CLICK_ADD_NEW);
    }

}