/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.net.Uri;

abstract class RingtoneHolder extends ItemHolder<Uri> {

    private final String mName;
    private boolean mSelected;
    private boolean mPlaying;
    private final RingtoneModel ringtoneModel;

    RingtoneHolder(final Uri uri, final String name, final RingtoneModel ringtoneModelParam) {
        super(uri);
        this.mName = name;
        this.ringtoneModel = ringtoneModelParam;
    }

    Uri getUri() {
        return this.getItem();
    }

    boolean isSilent() {
        return Uri.EMPTY.equals(this.getUri());
    }

    boolean isSelected() {
        return this.mSelected;
    }

    void setSelected(final boolean selected) {
        this.mSelected = selected;
    }

    boolean isPlaying() {
        return this.mPlaying;
    }

    void setPlaying(final boolean playing) {
        this.mPlaying = playing;
    }

    String getName() {
        return null != this.mName ? this.mName : this.ringtoneModel.getRingtoneTitle(this.getUri());
    }
}