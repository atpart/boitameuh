package fr.atpar.boitameuh.settings;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.preference.PreferenceFragmentCompat;

import fr.atpar.boitameuh.R;

/**
 * Populate from xml file the settings content. Very easy for simple preferences.
 * <p>
 * It should be public for its use by android framework.
 */
@SuppressWarnings("WeakerAccess")
public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(@Nullable final Bundle savedInstanceState, @Nullable final String rootKey) {
        this.setPreferencesFromResource(R.xml.root_preferences, rootKey);
    }
}
