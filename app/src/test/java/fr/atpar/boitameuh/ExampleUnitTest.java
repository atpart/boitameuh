package fr.atpar.boitameuh;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
class ExampleUnitTest {
    @Test
    void addition_isCorrect() {
        Assertions.assertEquals(4, 2 + 2);
    }
}